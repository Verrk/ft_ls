/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 12:53:04 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/16 23:44:28 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static t_file	*get_file(char *name)
{
	t_file		*new;

	new = (t_file*)malloc(sizeof(t_file));
	if (new)
	{
		new->name = ft_strdup(name);
		new->path = ft_strdup(name);
		new->stat = get_stat(name, NULL);
		new->files = NULL;
		new->prev = NULL;
		new->next = NULL;
	}
	return (new);
}

static void		get_len_arg_file(t_arg *arg, t_len *len, t_opt opt)
{
	while (arg)
	{
		if (arg->f)
		{
			len->len_lnk = ft_nbrlen(arg->stat->lnk) > len->len_lnk ?
				ft_nbrlen(arg->stat->lnk) : len->len_lnk;
			len->len_usr = ft_strlen(arg->stat->usr) > len->len_usr ?
				ft_strlen(arg->stat->usr) : len->len_usr;
			len->len_grp = ft_strlen(arg->stat->grp) > len->len_grp ?
				ft_strlen(arg->stat->grp) : len->len_grp;
			len->len_minor = ft_devlen(arg->stat->minor) > len->len_minor ?
				ft_devlen(arg->stat->minor) : len->len_minor;
			len->len_major = ft_devlen(arg->stat->major) > len->len_major ?
				ft_devlen(arg->stat->major) : len->len_major;
			len->len_size = ft_nbrlen(arg->stat->size) > len->len_size ?
				ft_nbrlen(arg->stat->size) : len->len_size;
		}
		arg = opt.r ? arg->prev : arg->next;
	}
}

void			display_arg_file(t_arg *arg, t_opt opt, t_bool *space)
{
	t_file		*file;
	t_len		len;

	init_len(&len);
	get_len_arg_file(arg, &len, opt);
	while (arg)
	{
		if (arg->f)
		{
			if (opt.l)
			{
				file = get_file(arg->name);
				ft_display_long(file, len);
				free_files(file, opt);
			}
			else
				ft_putendl(arg->name);
			*space = 1;
		}
		arg = opt.r ? arg->prev : arg->next;
	}
}

static void		ft_display_file(t_file *file, t_opt opt, t_len len)
{
	if (!opt.a && (file->name)[0] == '.')
		file = opt.r ? file->prev : file->next;
	else
	{
		if (opt.l)
			ft_display_long(file, len);
		else
			ft_putendl(file->name);
		file = opt.r ? file->prev : file->next;
	}
}

void			ft_display(t_file *dir, t_ls *ls)
{
	t_file		*tmp;
	t_len		len;

	tmp = dir->files;
	if (ls->nb_dir > 1 || ls->opt.rec)
	{
		if (ls->space)
			ft_putchar('\n');
		ft_putstr(dir->path);
		ft_putendl(":");
	}
	if (ls->opt.l)
		print_total(dir, ls->opt);
	init_len(&len);
	get_len(dir, &len, ls->opt);
	while (tmp)
	{
		ft_display_file(tmp, ls->opt, len);
		tmp = ls->opt.r ? tmp->prev : tmp->next;
		ls->space = 1;
	}
}
