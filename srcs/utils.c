/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/14 01:17:11 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/14 01:17:12 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int			arg_size(t_arg *arg)
{
	int		i;

	i = 0;
	while (arg)
	{
		arg = arg->next;
		i++;
	}
	return (i);
}

int			file_size(t_file *file)
{
	int		i;

	i = 0;
	while (file)
	{
		file = file->next;
		i++;
	}
	return (i);
}
