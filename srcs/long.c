/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 13:26:52 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/16 23:31:05 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static void		print_dev_size(t_file *file, t_len len)
{
	if (file->stat->minor || file->stat->major)
	{
		ft_nputchar(' ', len.len_major - ft_nbrlen(file->stat->major) + 1);
		ft_putnbr(file->stat->major);
		ft_putchar(',');
		ft_nputchar(' ', len.len_minor - ft_nbrlen(file->stat->minor) + 1);
		ft_putnbr(file->stat->minor);
	}
	else
	{
		if (len.len_major)
			ft_nputchar(' ', len.len_major + len.len_minor -
						ft_nbrlen(file->stat->size) + 2);
		else
			ft_nputchar(' ', len.len_size - ft_nbrlen(file->stat->size) + 1);
		if (len.len_major)
			ft_putchar(' ');
		ft_putnbr(file->stat->size);
	}
}

void			ft_display_long(t_file *file, t_len len)
{
	ft_putstr(file->stat->mode);
	ft_nputchar(' ', len.len_lnk - ft_nbrlen(file->stat->lnk) + 1);
	ft_putnbr(file->stat->lnk);
	ft_putchar(' ');
	ft_putstr(file->stat->usr);
	ft_nputchar(' ', len.len_usr - ft_strlen(file->stat->usr) + 1);
	ft_putstr(file->stat->grp);
	ft_nputchar(' ', len.len_grp - ft_strlen(file->stat->grp));
	print_dev_size(file, len);
	ft_putchar(' ');
	ft_putstr(file->stat->mtime);
	ft_putchar(' ');
	ft_putstr(file->name);
	if (file->stat->lnk_name[0])
	{
		ft_putstr(" -> ");
		ft_putstr(file->stat->lnk_name);
	}
	ft_putchar('\n');
}

void			init_len(t_len *len)
{
	len->len_lnk = 0;
	len->len_usr = 0;
	len->len_grp = 0;
	len->len_minor = 0;
	len->len_major = 0;
	len->len_size = 0;
}

void			get_len(t_file *dir, t_len *len, t_opt opt)
{
	t_file		*tmp;

	tmp = dir->files;
	while (tmp)
	{
		if (!opt.a && tmp->name[0] == '.')
			tmp = opt.r ? tmp->prev : tmp->next;
		else
		{
			len->len_lnk = ft_nbrlen(tmp->stat->lnk) > len->len_lnk ?
				ft_nbrlen(tmp->stat->lnk) : len->len_lnk;
			len->len_usr = ft_strlen(tmp->stat->usr) > len->len_usr ?
				ft_strlen(tmp->stat->usr) : len->len_usr;
			len->len_grp = ft_strlen(tmp->stat->grp) > len->len_grp ?
				ft_strlen(tmp->stat->grp) : len->len_grp;
			len->len_minor = ft_devlen(tmp->stat->minor) > len->len_minor ?
				ft_devlen(tmp->stat->minor) : len->len_minor;
			len->len_major = ft_devlen(tmp->stat->major) > len->len_major ?
				ft_devlen(tmp->stat->major) : len->len_major;
			len->len_size = ft_nbrlen(tmp->stat->size) > len->len_size ?
				ft_nbrlen(tmp->stat->size) : len->len_size;
			tmp = opt.r ? tmp->prev : tmp->next;
		}
	}
}

void			print_total(t_file *dir, t_opt opt)
{
	quad_t		total;
	t_file		*tmp;

	total = 0;
	tmp = dir->files;
	while (tmp)
	{
		if (!opt.a && tmp->name[0] == '.')
			tmp = opt.r ? tmp->prev : tmp->next;
		else
		{
			total += tmp->stat->blocks;
			tmp = opt.r ? tmp->prev : tmp->next;
		}
	}
	ft_putstr("total ");
	ft_putnbr(total);
	ft_putchar('\n');
}
