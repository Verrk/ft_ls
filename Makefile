#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/02 10:47:13 by cpestour          #+#    #+#              #
#    Updated: 2015/01/16 23:20:39 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC=gcc
CFLAGS=-Wall -Werror -Wextra -g -Ilibft/includes -Iincludes
LDFLAGS=-Llibft -lft
SRC=srcs/main.c srcs/ft_ls.c srcs/opt.c srcs/dir.c srcs/free.c \
srcs/display.c srcs/stat.c srcs/sort.c srcs/long.c srcs/utils.c
OBJ=$(SRC:.c=.o)
NAME=ft_ls

all: lib $(NAME)

lib:
	make -C libft
	@echo "OK !"

$(NAME): $(OBJ)
	@$(CC) -o $@ $^ $(LDFLAGS)
	@echo "\t\t\t\t\t\t\t\t\t\033[33;32mft_ls done\033[33;37m"

%.o: %.c
	@$(CC) -o $@ -c $< $(CFLAGS)
	@echo -n "\033[33;32m.\033[33;37m"

clean:
	make clean -C libft
	rm -f *~ srcs/*~ includes/*~ $(OBJ)

fclean: clean
	make fclean -C libft
	rm -f $(NAME)

re: fclean all
