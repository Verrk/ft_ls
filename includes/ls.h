/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/02 11:18:50 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/16 23:45:55 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LS_H
# define LS_H

# include "libft.h"
# include <sys/types.h>
# include <sys/stat.h>
# include <dirent.h>
# include <errno.h>
# include <pwd.h>
# include <grp.h>
# include <uuid/uuid.h>
# include <time.h>

typedef struct dirent	t_dirent;
typedef unsigned char	t_bool;

typedef struct			s_len
{
	unsigned int		len_lnk;
	unsigned int		len_usr;
	unsigned int		len_grp;
	unsigned int		len_minor;
	unsigned int		len_major;
	unsigned int		len_size;
}						t_len;

typedef struct			s_stat
{
	char				*mode;
	int					lnk;
	char				*usr;
	char				*grp;
	int					major;
	int					minor;
	off_t				size;
	char				*mtime;
	time_t				time;
	char				*lnk_name;
	quad_t				blocks;
}						t_stat;

typedef struct			s_opt
{
	int					a;
	int					r;
	int					t;
	int					rec;
	int					l;
}						t_opt;

typedef struct			s_file
{
	char				*name;
	char				*path;
	t_stat				*stat;
	struct s_file		*files;
	struct s_file		*prev;
	struct s_file		*next;
}						t_file;

typedef struct			s_arg
{
	char				*name;
	t_bool				f;
	t_stat				*stat;
	struct s_arg		*prev;
	struct s_arg		*next;
}						t_arg;

typedef struct			s_ls
{
	t_opt				opt;
	t_arg				*arg;
	int					nb_dir;
	t_bool				space;
	int					status;
}						t_ls;

void					ft_ls(char *name, char *path, t_ls *ls);
int						get_opt(char **av, t_ls *ls);
void					get_arg(char *name, t_ls *ls);
void					sort_arg(t_ls *ls);
int						arg_size(t_arg *arg);
void					sort_file(t_file *dir, t_opt opt);
int						file_size(t_file *file);
void					push_file(t_file *dir, t_dirent *file);
t_stat					*get_stat(char *path, t_dirent *file);
void					ft_display(t_file *dir, t_ls *ls);
void					display_arg_file(t_arg *arg, t_opt opt, t_bool *space);
void					ft_display_long(t_file *file, t_len len);
void					print_total(t_file *dir, t_opt opt);
void					init_len(t_len *len);
void					get_len(t_file *dir, t_len *len, t_opt opt);
void					free_ls(t_ls *ls);
void					free_dir(t_file *dir, t_opt opt);
void					free_files(t_file *file, t_opt opt);

#endif
