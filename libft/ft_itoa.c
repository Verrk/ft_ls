/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 14:56:01 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/03 18:33:45 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_abs(int n)
{
	int		ret;

	ret = (n < 0) ? -n : n;
	return (ret);
}

char		*ft_itoa(int n)
{
	char	*s;
	size_t	i;

	i = ft_nbrlen(n);
	i += (n < 0) ? 1 : 0;
	s = (char *)malloc(i + 1);
	s[i] = '\0';
	if (n < 0)
		s[0] = '-';
	if (n == 0)
		s[0] = '0';
	while (i-- && n)
	{
		s[i] = ft_abs(n % 10) + '0';
		n /= 10;
	}
	return (s);
}
