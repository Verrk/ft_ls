/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 15:01:23 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/08 15:22:59 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_abs(quad_t n)
{
	quad_t	ret;

	ret = (n < 0) ? -n : n;
	return (ret);
}

void		ft_putnbr(quad_t n)
{
	if (n < 0)
		write(1, "-", 1);
	if (n >= 10 || n <= -10)
	{
		ft_putnbr(ft_abs(n / 10));
		ft_putnbr(ft_abs(n % 10));
	}
	else
		ft_putchar(ft_abs(n) + '0');
}
